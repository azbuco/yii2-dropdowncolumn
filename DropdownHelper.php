<?php

namespace azbuco\dropdowncolumn;

use azbuco\softdelete\behaviors\SoftDelete;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Inflector;
use yii\helpers\Url;

class DropdownHelper
{

    public static function divider()
    {
        return '<li role="presentation" class="divider"></li>';
    }

    public static function header($header)
    {
        return '<li class="dropdown-header">' . $header . '</li>';
    }

    public static function viewItem()
    {
        return function($model, $key, $index) {
            return [
                'label' => Yii::t('yii', 'View'),
                'url' => Url::to(['view', 'id' => $model->id]),
            ];
        };
    }

    public static function updateItem()
    {
        return function($model, $key, $index) {
            return [
                'label' => Yii::t('yii', 'Update'),
                'url' => Url::to(['update', 'id' => $model->id]),
            ];
        };
    }

    public static function deleteItem($reload = '.grid-view tbody')
    {
        return function($model, $key, $index) use ($reload) {
            return [
                'label' => Yii::t('yii', 'Delete'),
                'url' => Url::to(['delete', 'id' => $model->id]),
                'linkOptions' => [
                    'data-app-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                    'data-app-method' => 'post',
                    'data-app-reload' => $reload,
                ]
            ];
        };
    }

    public static function softDeleteItem($reload = '.grid-view tbody')
    {
        return function($model, $key, $index) use ($reload) {
            /* @var $model ActiveRecord */
            $visible = true;
            $isSoftDeletable = false;
            foreach ($model->getBehaviors() as $behavior) {
                if ($behavior instanceof SoftDelete) {
                    $visible = !$model->isDeleted();
                    $isSoftDeletable = true;
                }
            }

            if ($isSoftDeletable === false) {
                throw new \yii\base\InvalidCallException('The class "' . get_class($model) . '" must be soft deletable! Is the correct behaviours added?');
            }

            $controller = Inflector::camel2id(basename(get_class($model)));

            return [
                'label' => Yii::t('yii', 'Delete'),
                'url' => Url::to([$controller . '/delete', 'id' => $model->id, 'redirectTo' => Yii::$app->request->url]),
                'linkOptions' => [
                    'data-app-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                    'data-app-method' => 'post',
                    'data-app-reload' => $reload,
                ],
                'visible' => $visible,
            ];
        };
    }

    public static function restoreItem($reload = '.grid-view tbody')
    {

        return function($model, $key, $index) use ($reload) {
            /* @var $model ActiveRecord */
            $visible = true;
            foreach ($model->getBehaviors() as $behavior) {
                if ($behavior instanceof SoftDelete) {
                    $visible = $model->isDeleted();
                }
            }
            return [
                'label' => Yii::t('yii', 'Restore'),
                'url' => Url::to([Yii::$app->controller->id . '/restore', 'id' => $model->id]),
                'linkOptions' => [
                    'data-app-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                    'data-app-method' => 'post',
                    'data-app-reload' => $reload,
                ],
                'visible' => $visible,
            ];
        };
    }

    public static function printItem()
    {
        return function($model, $key, $index) {
            return [
                'label' => Yii::t('yii', 'Print'),
                'url' => Url::to(['print', 'id' => $model->id]),
            ];
        };
    }
    
    public static function modalUpdateItem($trigger)
    {
        return function($model, $key, $index) use ($trigger) {
            $controller = Inflector::camel2id(basename(get_class($model)));

            return [
                'label' => Yii::t('yii', 'Update'),
                'url' => Url::to([$controller . '/update', 'id' => $model->id]),
                'linkOptions' => [
                    'data-trigger' => $trigger,
                ],
            ];
        };
    }
}
