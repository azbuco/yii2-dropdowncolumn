<?php

namespace azbuco\dropdowncolumn;

use Closure;
use yii\grid\Column;
use yii\helpers\Inflector;

class DropdownColumn extends Column {

    /**
     * @var string
     */
    public $label = '<span class="caret"></span>';
    public $template = '<div class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">{label}</a>{dropdown}</div>';
    public $disabledTemplate = '';
    public $items = [];
    public $visibleItems = [];
    public $dropdownOptions = [];
    public $enabled = true;
    public $bootstrapVersion = 4;

    /**
     * @inheritdoc
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        if ($this->enabled === false) {
            return $this->disabledTemplate;
        }

        if ($this->enabled instanceof Closure) {
            $enabled = call_user_func($this->enabled, $model, $key, $index);
            if (!$enabled) {
                return $this->disabledTemplate;
            }
        }

        return preg_replace_callback("/{(\\w+)}/", function ($matches) use ($model, $key, $index) {
            $renderMethod = 'render' . Inflector::id2camel($matches[1]);
            if (!method_exists($this, $renderMethod)) {
                return $matches[0];
            }
            return $this->$renderMethod($model, $key, $index);
        }, $this->template);
    }

    public function renderLabel($model, $key, $index)
    {
        if ($this->label instanceof \Closure) {
            $func = $this->label;
            return $func($model, $key, $index);
        }

        return $this->label;
    }

    public function renderDropdown($model, $key, $index)
    {
        if ($this->items instanceof \Closure) {
            $func = $this->items;
            $items = $func($model, $key, $index);
        } else {
            $items = [];
        }

        foreach ($this->items as $name => $cfg) {
            if ($cfg instanceof Closure) {
                $item = $cfg($model, $key, $index);
            } else {
                $item = $cfg;
            }

            $isVisible = true;

            if (isset($this->visibleItems[$name])) {
                if ($this->visibleItems[$name] instanceof Closure) {
                    $isVisible = call_user_func($this->visibleItems[$name], $model, $key, $index);
                } else {
                    $isVisible = $this->visibleItems[$name];
                }
            }

            $items[] = $item;
        }

        $this->dropdownOptions['items'] = $items;

        switch ($this->bootstrapVersion) {
            case 3:
                return \yii\bootstrap\Dropdown::widget($this->dropdownOptions);
            case 4:
                return \yii\bootstrap4\Dropdown::widget($this->dropdownOptions);
            default: 
                return 'Invalid bootstrap version';
        }
    }

}
